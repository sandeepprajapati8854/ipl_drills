const csv = require("csv-parser");
const fs = require("fs");

function extraRun() {
  const iplMatch = [];
  const iplDeliveries = [];

  const outputFilePath =
    (__dirname, "./src/public/output/3-extra-runs-conceded-per-team.json");

  const inputDataMatchesPath = (__dirname, "./src/data/matches.csv");
  const inputDeliveriesFilePath = (__dirname, "./src/data/deliveries.csv");

  fs.createReadStream(inputDataMatchesPath)
    .pipe(csv())
    .on("data", (data) => iplMatch.push(data))
    .on("end", () => {
      const match2016 = iplMatch.filter(function (ele) {
        return ele.season == 2016;
      });

      const store = match2016.map(function (ele) {
        return ele.id;
        //  console.log(ele.id);
      });
      fs.createReadStream(inputDeliveriesFilePath)
        .pipe(csv())
        .on("data", (data) => iplDeliveries.push(data))
        .on("end", () => {
          const extraRunS = iplDeliveries.reduce(function (acc, curr) {
            if (store.includes(curr.match_id)) {
              if (acc[curr.bowling_team]) {
                acc[curr.bowling_team] += Number(curr.extra_runs);
              } else {
                acc[curr.bowling_team] = Number(curr.extra_runs);
              }
            }
            return acc;
          }, {});
          //    console.log(extraRunS);
          fs.writeFile(outputFilePath, JSON.stringify(extraRunS), (error) => {
            if (error) {
              throw error;
            } else {
              console.log("File written successfully");
            }
          });
        });
    });
}
extraRun();

const csv = require("csv-parser");
const fs = require("fs");

function topEconomicalBowlers() {
  let matchData = [];
  let deliverieData = [];

  const outputFilePath =
    (__dirname, "./src/public/output/4-top-10-economical-bowlers.json");

  const inputDataMatchesPath = (__dirname, "./src/data/matches.csv");
  const inputDeliveriesFilePath = (__dirname, "./src/data/deliveries.csv");

  fs.createReadStream(inputDataMatchesPath)
    .pipe(csv())
    .on("data", (data) => matchData.push(data))
    .on("end", () => {
      //  console.log(matchData); // all match data is here.
      let economicesYear2015 = matchData.filter(function (ele) {
        return ele.season == 2015;
      });

      let storeID = economicesYear2015.map(function (ele) {
        return ele.id;
      });
      //   console.log(storeID); //for checking

      fs.createReadStream(inputDeliveriesFilePath)
        .pipe(csv())
        .on("data", (data) => deliverieData.push(data))
        .on("end", () => {
          // console.log(deliverieData); // all deliveries data is here..
          let bowlersRun = deliverieData.reduce(function (acc, curr) {
            if (storeID.includes(curr.match_id)) {
              if (acc[curr.bowler]) {
                acc[curr.bowler] = acc[curr.bowler] + Number(curr.total_runs);
              } else {
                acc[curr.bowler] = Number(curr.total_runs);
                //  console.log(acc[curr.bowler]);
              }
            }
            return acc;
          }, {});
          //  console.log(bowlersRun);
          //console.log(typeof bowlersRun);
          //   console.log(Object.keys(bowlersRun).length); // find the length of objects.

          // find out, bowlers balls
          const bowlersBalls = deliverieData.reduce((acc, curr) => {
            if (storeID.includes(curr.match_id)) {
              if (acc[curr.bowler]) {
                if (curr.wide_runs == 0 && curr.noball_runs == 0) {
                  acc[curr.bowler] = acc[curr.bowler] + 1;
                } else {
                  acc[curr.bowler] = acc[curr.bowler] + 0;
                }
              } else {
                if (curr.wide_runs == 0 && curr.noball_runs == 0) {
                  acc[curr.bowler] = 1;
                } else {
                  acc[curr.bowler] = 0;
                }
              }
            }
            return acc;
          }, {});

          const bowlerEconomy = deliverieData.reduce((acc, curr) => {
            if (storeID.includes(curr.match_id)) {
              acc[curr.bowler] = Number(
                (
                  bowlersRun[curr.bowler] /
                  (bowlersBalls[curr.bowler] / 6)
                ).toFixed(2)
              );
            }
            return acc;
          }, {});
          //   console.log(bowlerEconomy);

          let sortable = [];
          for (var key in bowlerEconomy) {
            sortable.push([key, bowlerEconomy[key]]);
          }

          sortable.sort(function (a, b) {
            return a[1] - b[1];
          });
          //  console.log(sortable);
          finalData = [];
          //  console.log(sortable.length);
          for (let i = 0; i < 10; i++) {
            finalData.push(sortable[i]);
          }
          //  console.log(finalData);
          fs.writeFile(outputFilePath, JSON.stringify(finalData), (error) => {
            if (error) {
              throw error;
            } else {
              console.log("File written successfully");
            }
          });
        });
    });
}

topEconomicalBowlers();

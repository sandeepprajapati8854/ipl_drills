const csv = require("csv-parser");
const fs = require("fs");
const results = [];

const outputFilePath =
  (__dirname, "./src/public/output/2-matches-won-per-team-per-year.json");

const inputDatafilePath = (__dirname, "./src/data/matches.csv");

fs.createReadStream(inputDatafilePath)
  .pipe(csv())
  .on("data", (data) => results.push(data))
  .on("end", () => {
    let matches = {};
    for (let index in results) {
      let ses = results[index].season;
      let win = results[index].winner;
      if (matches.hasOwnProperty(ses)) {
        if (matches[ses][win]) {
          matches[ses][win]++;
        } else {
          matches[ses][win] = 1;
        }
      } else {
        matches[ses] = {};
      }
    }

    console.log(matches);
    fs.writeFile(outputFilePath, JSON.stringify(matches), (error) => {
      if (error) {
        throw error;
      } else {
        console.log("Sucessfully written");
      }
    });
  });

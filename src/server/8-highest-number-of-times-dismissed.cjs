const csv = require("csv-parser");
const fs = require("fs");

function highestPlayerDismissed() {
  const deliveriesData = [];

  const outputFilePath =
    (__dirname, "./src/public/output/8-highest-player-dismissed.json");
  const diliveriesFilePath = (__dirname, "./src/data/deliveries.csv");

  fs.createReadStream(diliveriesFilePath)
    .pipe(csv())
    .on("data", (data) => deliveriesData.push(data))
    .on("end", () => {
      const allDismissedPlayer = deliveriesData.reduce((acc, curr) => {
        if (curr.player_dismissed) {
          if (acc[curr.bowler]) {
            if (acc[curr.bowler].hasOwnProperty(curr.batsman)) {
              acc[curr.bowler][curr.batsman] += 1;
            } else {
              acc[curr.bowler][curr.batsman] = 1;
            }
          } else {
            acc[curr.bowler] = {};
          }
        }
        return acc;
      }, {});

      //  console.log(allDismissedPlayer);

      const bowlerArrayConvert = Object.entries(allDismissedPlayer);
      const bowlerDismissSortArr = bowlerArrayConvert.reduce((acc, curr) => {
        const sortedPlayers = Object.entries(curr[1]).sort(
          (start, end) => end[1] - start[1]
        );
        acc[curr[0]] = sortedPlayers[0];
        return acc;
      }, {});

      console.log(bowlerDismissSortArr);
      const res = Object.fromEntries(
        Object.entries(bowlerDismissSortArr)
          .filter((value) => value[1])
          .sort((start, end) => end[1][1] - start[1][1])
          .slice(0, 1)
      );

      fs.writeFile(outputFilePath, JSON.stringify(res), (error) => {
        if (error) {
          throw error;
        } else {
          console.log("File written successfully");
        }
      });
    });
}

highestPlayerDismissed();

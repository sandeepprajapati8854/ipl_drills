const csv = require("csv-parser");
const fs = require("fs");
const results = [];

const outputFilePath =
  (__dirname, "./src/public/output/1-matches-per-year.json");

const inputDatafilePath = (__dirname, "./src/data/matches.csv");

fs.createReadStream(inputDatafilePath)
  .pipe(csv())
  .on("data", (data) => results.push(data))
  .on("end", () => {
    let res = results.reduce(function (acc, curr) {
      if (acc[curr.season]) {
        acc[curr.season] += 1;
      } else {
        acc[curr.season] = 1;
      }
      return acc;
    }, {});
    //  console.log(res);
    fs.writeFile(outputFilePath, JSON.stringify(res), (error) => {
      if (error) {
        throw error;
      } else {
        console.log("Sucessfully written");
      }
    });
  });

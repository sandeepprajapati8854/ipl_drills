const csv = require("csv-parser");
const fs = require("fs");
const { Session } = require("inspector");

function playerOfTheMatch() {
  let matchData = [];
  let seasonArray = [];
  let obj = {};
  let answer = [];

  const outputFilePath =
    (__dirname,
    "./src/public/output/6-player-who-has-own-heighest-number-of-player-of-the-match.json");

  const inputDataMatchesPath = (__dirname, "./src/data/matches.csv");

  fs.createReadStream(inputDataMatchesPath)
    .pipe(csv())
    .on("data", (data) => matchData.push(data))
    .on("end", () => {
      matchData.map(function (ele) {
        let runningData = ele;
        seasonArray.push([runningData.season, runningData.player_of_match]);
      });
      //    console.log(seasonArray);

      seasonArray.map(function (ele, index) {
        if (obj[seasonArray[index][0]]) {
          if (obj[seasonArray[index][0]][seasonArray[index][1]]) {
            obj[seasonArray[index][0]][seasonArray[index][1]]++;
          } else {
            obj[seasonArray[index][0]][seasonArray[index][1]] = 1;
          }
        } else {
          obj[seasonArray[index][0]] = { [seasonArray[index][1]]: 1 };
        }
      });
      //   console.log(obj);

      for (let year in obj) {
        let temp = 0;
        let name;
        for (let val in obj[year]) {
          if (obj[year][val] > temp) {
            temp = obj[year][val];
            name = val;
          }
        }
        answer.push(name, temp);
        temp = 0;
        name = "";
      }
      console.log(answer);
      fs.writeFile(outputFilePath, JSON.stringify(answer), (error) => {
        if (error) {
          throw error;
        } else {
          console.log("File written successfully");
        }
      });
    });
}

playerOfTheMatch();

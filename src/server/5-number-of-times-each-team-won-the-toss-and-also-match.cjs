const csv = require("csv-parser");
const fs = require("fs");

function printTeam() {
  let matchData = [];
  matchAndTossWinner = {};

  const outputFilePath =
    (__dirname,
    "./src/public/output/5-number-of-times-each-team-won-the-toss-and-also-match.json");

  const inputDataMatchesPath = (__dirname, "./src/data/matches.csv");

  fs.createReadStream(inputDataMatchesPath)
    .pipe(csv())
    .on("data", (data) => matchData.push(data))
    .on("end", () => {
      let final = matchData.map((matchObj) => {
        let tosWin = matchObj.toss_winner;
        let win = matchObj.winner;
        if (tosWin == win) {
          if (matchAndTossWinner[win]) {
            matchAndTossWinner[win] += 1;
          } else {
            matchAndTossWinner[win] = 1;
          }
        }
        // return final; // no need f
      });
      console.log(matchAndTossWinner); // all value will store in matchAndTossinner
      fs.writeFile(
        outputFilePath,
        JSON.stringify(matchAndTossWinner),
        (error) => {
          if (error) {
            throw error;
          } else {
            console.log("File written successfully");
          }
        }
      );
    });
}
printTeam();

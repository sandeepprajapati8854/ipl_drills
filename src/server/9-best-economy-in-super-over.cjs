const csv = require("csv-parser");
const fs = require("fs");

function bestEconomyInSuperOver() {
  const deliveriesArray = [];

  const outputFilePath =
    (__dirname, "./src/public/output/9-best-economy-in-super-over.json");
  const deliveriesFilePath = (__dirname, "./src/data/deliveries.csv");

  fs.createReadStream(deliveriesFilePath)
    .pipe(csv())
    .on("data", (data) => deliveriesArray.push(data))
    .on("end", () => {
      const superOverArray = deliveriesArray
        .filter((element) => {
          return element.is_super_over == "1";
        })
        .reduce((acc, curr) => {
          if (curr.wide_runs == "0" && curr.noball_runs == "0") {
            if (acc[curr.bowler]) {
              acc[curr.bowler][0] += Number(curr.batsman_runs);
              acc[curr.bowler][1] += 1;
            } else {
              acc[curr.bowler] = [Number(curr.batsman_runs), 1];
            }
          } else {
            if (acc[curr.bowler]) {
              acc[curr.bowler[0]] = Number(curr.total_runs) + 1;
            } else {
              acc[curr.bowler] = [Number(curr.total_runs), 0];
            }
          }
          return acc;
        }, {});

      //  console.log(superOverArray);

      const bowlerEconomy = Object.entries(superOverArray)
        .map((element) => {
          element[1] = Math.round(element[1][0] / (element[1][1] / 6));
          return element;
        })
        .sort((start, end) => {
          if (start[1] < end[1]) {
            return -1;
          } else {
            return 1;
          }
        });
      console.log(bowlerEconomy[0]);

      fs.writeFile(
        outputFilePath,
        JSON.stringify(bowlerEconomy[0]),
        (error) => {
          if (error) {
            throw error;
          } else {
            console.log("File written successfully");
          }
        }
      );
    });
}

bestEconomyInSuperOver();

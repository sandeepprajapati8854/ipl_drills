const csv = require("csv-parser");
const fs = require("fs");

function batsmanStrikeRateOfEachSeason() {
  const matchesData = [];
  const deliveriesData = [];

  const outputFilePath =
    (__dirname,
    "./src/public/output/7-Find-the-strike-rate-of-a-batsman-for-each-season.json");
  const matchesFilePath = (__dirname, "./src/data/matches.csv");
  const diliveriesFilePath = (__dirname, "./src/data/deliveries.csv");

  fs.createReadStream(matchesFilePath)
    .pipe(csv())
    .on("data", (data) => matchesData.push(data))
    .on("end", () => {
      //  console.log(matchesData); //checking for data
      const seasonMatchedId = matchesData.reduce(function (acc, curr) {
        acc[curr.id] = curr.season;
        return acc;
      }, {});
      //  console.log(seasonMatchedId);

      fs.createReadStream(diliveriesFilePath)
        .pipe(csv())
        .on("data", (data) => deliveriesData.push(data))
        .on("end", () => {
          //  console.log(deliveriesData); // checking for data.
          const batsmanStrikeRate = deliveriesData.reduce((acc, curr) => {
            const seasonList = seasonMatchedId[curr.match_id];
            if (acc[curr.batsman]) {
              if (acc[curr.batsman].hasOwnProperty(seasonList)) {
                acc[curr.batsman][seasonList].totalRuns += Number(
                  curr.batsman_runs
                );
                acc[curr.batsman][seasonList].totalBalls += 1;
              } else {
                acc[curr.batsman][seasonList] = {};
                acc[curr.batsman][seasonList].totalRuns = Number(
                  curr.batsman_runs
                );
                acc[curr.batsman][seasonList].totalBalls = 1;
              }
            } else {
              acc[curr.batsman] = {};
              acc[curr.batsman][seasonList] = {};
              acc[curr.batsman][seasonList].totalRuns = Number(curr.total_runs);
              acc[curr.batsman][seasonList].totalBalls = 1;
            }
            return acc;
          }, {});
          // console.log(batsmanStrikeRate);
          let finalbatsmanStrikeRate = {};
          Object.keys(batsmanStrikeRate).map(function (ele1) {
            let strikeRate = {};
            Object.keys(batsmanStrikeRate[ele1]).map(function (ele2) {
              let calculationOfStrikeRate = (
                (batsmanStrikeRate[ele1][ele2].totalRuns /
                  batsmanStrikeRate[ele1][ele2].totalBalls) *
                100
              ).toFixed(2);
              strikeRate[ele2] = Number(calculationOfStrikeRate);
            });
            finalbatsmanStrikeRate[ele1] = strikeRate;
          });
          //    console.log(finalbatsmanStrikeRate);
          fs.writeFile(
            outputFilePath,
            JSON.stringify(finalbatsmanStrikeRate),
            (error) => {
              if (error) {
                throw error;
              } else {
                console.log("File written successfully");
              }
            }
          );
        });
    });
}

batsmanStrikeRateOfEachSeason();

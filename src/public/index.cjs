fetch("./output/1-matches-per-year.json")
  .then((data) => data.json())
  .then((data) => {
    const problem1Data = Object.values(data);
    const key = Object.keys(data);

    const chart = Highcharts.chart("container", {
      title: {
        text: "Matches Per Year",
        align: "center",
      },
      subtitle: {
        text: "",
        align: "left",
      },
      xAxis: {
        categories: key,
      },
      series: [
        {
          type: "column",
          name: "Matches",
          colorByPoint: true,
          data: problem1Data,
          showInLegend: false,
        },
      ],
    });

    document.getElementById("plain").addEventListener("click", () => {
      chart.update({
        chart: {
          inverted: false,
          polar: false,
        },
        subtitle: {
          text: "",
        },
      });
    });

    document.getElementById("inverted").addEventListener("click", () => {
      chart.update({
        chart: {
          inverted: true,
          polar: false,
        },
        subtitle: {
          text: "",
        },
      });
    });

    document.getElementById("polar").addEventListener("click", () => {
      chart.update({
        chart: {
          inverted: false,
          polar: true,
        },
        subtitle: {
          text: "",
        },
      });
    });
  });

fetch("./output/2-matches-won-per-team-per-year.json")
  .then((data) => data.json())
  .then((data) => {
    const keys = Object.keys(data);
    const years = Object.keys(data).length;
    const entriesData = Object.entries(data);

    let index;
    let dataMatches = {};
    entriesData.map((element) => {
      const teams = Object.entries(element[1]);

      teams.map((item) => {
        if (dataMatches[item[0]]) {
          index = keys.indexOf(element[0]);
          dataMatches[item[0]][index] = item[1];
        } else {
          dataMatches[item[0]] = [];
          dataMatches[item[0]].length = years;
          dataMatches[item[0]].fill(0);

          index = keys.indexOf(element[0]);
          dataMatches[item[0]][index] = item[1];
        }
      });
    });

    let plotData = [];
    Object.keys(dataMatches).map((element) => {
      let dataObject = {};
      dataObject["name"] = element;
      dataObject["data"] = dataMatches[element];
      plotData.push(dataObject);
    });

    Highcharts.chart("container2", {
      chart: {
        type: "area",
      },
      title: {
        text: "Matches won per team per year",
        align: "left",
      },
      subtitle: {
        text: "",
        align: "left",
      },
      yAxis: {
        title: {
          useHTML: true,
          text: "",
        },
      },
      tooltip: {
        shared: true,
        headerFormat:
          '<span style="font-size:12px"><b>{point.key}</b></span><br>',
      },
      plotOptions: {
        series: {
          pointStart: 2012,
        },
        area: {
          stacking: "normal",
          lineColor: "#666666",
          lineWidth: 1,
          marker: {
            lineWidth: 1,
            lineColor: "#666666",
          },
        },
      },
      series: plotData,
    });
  });

fetch("./output/3-extra-runs-conceded-per-team.json")
  .then((data) => data.json())
  .then((data) => {
    let problem3Data = Object.entries(data);

    Highcharts.chart("container3", {
      chart: {
        type: "pie",
        options3d: {
          enabled: true,
          alpha: 45,
        },
      },
      title: {
        text: "Extra Run Conceded Per team",
        align: "center",
      },
      subtitle: {
        text: "",
        align: "left",
      },
      plotOptions: {
        pie: {
          innerSize: 100,
          depth: 45,
        },
      },
      series: [
        {
          name: "Run",
          data: problem3Data,
        },
      ],
    });
  });

fetch("./output/4-top-10-economical-bowlers.json")
  .then((data) => data.json())
  .then((data) => {
    let problem4Data = Object.fromEntries(data);
    let listEntries = Object.entries(problem4Data);

    Highcharts.chart("container4", {
      chart: {
        type: "pie",
        options3d: {
          enabled: true,
          alpha: 45,
        },
      },
      title: {
        text: "Top ten economical bowler",
        align: "center",
      },
      subtitle: {
        text: "",
        align: "left",
      },
      plotOptions: {
        pie: {
          innerSize: 100,
          depth: 45,
        },
      },
      series: [
        {
          name: "Bowler",
          data: listEntries,
        },
      ],
    });
  });

fetch("./output/5-number-of-times-each-team-won-the-toss-and-also-match.json")
  .then((data) => data.json())
  .then((data) => {
    let listEntries = Object.entries(data);

    Highcharts.chart("container5", {
      chart: {
        type: "pie",
        options3d: {
          enabled: true,
          alpha: 45,
        },
      },
      title: {
        text: "Number of times each team won the toss and also match",
        align: "center",
      },
      subtitle: {
        text: "",
        align: "left",
      },
      plotOptions: {
        pie: {
          innerSize: 100,
          depth: 45,
        },
      },
      series: [
        {
          name: "times",
          data: listEntries,
        },
      ],
    });
  });

fetch(
  "./output/6-player-who-has-own-heighest-number-of-player-of-the-match.json"
)
  .then((data) => data.json())
  .then((data) => {
    const playerOfMatches = data.filter((playersData) => {
      return typeof playersData === "number";
    });

    const playerNames = data.filter((playersData) => {
      return typeof playersData === "string";
    });

    // console.log(key);
    const chart = Highcharts.chart("container6", {
      title: {
        text: "Player who has own heighest number of player of the match",
        align: "center",
      },
      subtitle: {
        text: "",
        align: "left",
      },
      xAxis: {
        categories: playerNames,
      },
      series: [
        {
          type: "column",
          name: "Player of match",
          colorByPoint: true,
          data: playerOfMatches,
          showInLegend: false,
        },
      ],
    });

    document.getElementById("plain").addEventListener("click", () => {
      chart.update({
        chart: {
          inverted: false,
          polar: false,
        },
        subtitle: {
          text: "",
        },
      });
    });

    document.getElementById("inverted").addEventListener("click", () => {
      chart.update({
        chart: {
          inverted: true,
          polar: false,
        },
        subtitle: {
          text: "",
        },
      });
    });

    document.getElementById("polar").addEventListener("click", () => {
      chart.update({
        chart: {
          inverted: false,
          polar: true,
        },
        subtitle: {
          text: "",
        },
      });
    });
  });

fetch("./output/7-Find-the-strike-rate-of-a-batsman-for-each-season.json")
  .then((data) => data.json())
  .then((data) => {
    const keys = Object.keys(data);
    const years = Object.keys(data).length;
    const entriesData = Object.entries(data);

    let index;
    let dataMatches = {};
    entriesData.map((element) => {
      const teams = Object.entries(element[1]);

      teams.map((item) => {
        if (dataMatches[item[0]]) {
          index = keys.indexOf(element[0]);
          dataMatches[item[0]][index] = item[1];
        } else {
          dataMatches[item[0]] = [];
          dataMatches[item[0]].length = years;
          dataMatches[item[0]].fill(0);

          index = keys.indexOf(element[0]);
          dataMatches[item[0]][index] = item[1];
        }
      });
    });

    let plotData = [];
    Object.keys(dataMatches).map((element) => {
      let dataObject = {};
      dataObject["name"] = element;
      dataObject["data"] = dataMatches[element];
      plotData.push(dataObject);
    });

    Highcharts.chart("container7", {
      chart: {
        type: "area",
      },
      title: {
        text: "Strike rate of the batsman per each season",
        align: "left",
      },
      subtitle: {
        text: "",
        align: "left",
      },
      yAxis: {
        title: {
          useHTML: true,
          text: "",
        },
      },
      tooltip: {
        shared: true,
        headerFormat:
          '<span style="font-size:12px"><b>{point.key}</b></span><br>',
      },
      plotOptions: {
        series: {
          pointStart: 2012,
        },
        area: {
          stacking: "normal",
          lineColor: "#666666",
          lineWidth: 1,
          marker: {
            lineWidth: 1,
            lineColor: "#666666",
          },
        },
      },
      series: plotData,
    });
  });

fetch("./output/8-highest-player-dismissed.json")
  .then((data) => data.json())
  .then((data) => {
    let key = Object.entries(data);
    let value = Object.keys(data);

    Highcharts.chart("container8", {
      chart: {
        type: "column",
      },
      title: {
        text: "Highest player dismissed",
      },
      subtitle: {
        text: "",
      },
      xAxis: {
        type: "category",
        labels: {
          rotation: 0,
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif",
          },
        },
      },
      yAxis: {
        min: 0,
        title: {
          text: value,
        },
      },
      legend: {
        enabled: false,
      },
      tooltip: {
        pointFormat: "",
      },
      series: [
        {
          name: "",
          data: key[0],
          dataLabels: {
            enabled: true,
            rotation: -90,
            color: "#FFFFFF",
            align: "right",
            format: "{point.y:.1f}",
            y: 10,
            style: {
              fontSize: "13px",
              fontFamily: "Verdana, sans-serif",
            },
          },
        },
      ],
    });
  });

fetch("./output/9-best-economy-in-super-over.json")
  .then((data) => data.json())
  .then((data) => {
    const bestEconomyP = Object.entries(Object.fromEntries([data]));
    console.log(bestEconomyP);
    Highcharts.chart("container9", {
      chart: {
        type: "column",
      },
      title: {
        text: "Best Economy in super over",
      },
      subtitle: {
        text: "",
      },
      xAxis: {
        type: "category",
        labels: {
          rotation: 0,
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif",
          },
        },
      },
      yAxis: {
        min: 0,
        title: {
          text: "",
        },
      },
      legend: {
        enabled: false,
      },
      tooltip: {
        pointFormat: "",
      },
      series: [
        {
          name: "Population",
          data: bestEconomyP,
          dataLabels: {
            enabled: true,
            rotation: -90,
            color: "#FFFFFF",
            align: "right",
            format: "{point.y:.1f}",
            y: 10,
            style: {
              fontSize: "13px",
              fontFamily: "Verdana, sans-serif",
            },
          },
        },
      ],
    });
  });
